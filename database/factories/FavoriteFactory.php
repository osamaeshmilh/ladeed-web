<?php

/** @var Factory $factory */

use App\Models\Favorite;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Favorite::class, function (Faker $faker) {
    return [
        'food_id' => $faker->numberBetween(1, 30),
        'user_id' => $faker->numberBetween(1, 6)
    ];
});
