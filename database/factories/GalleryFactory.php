<?php

/** @var Factory $factory */

use App\Models\Gallery;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Gallery::class, function (Faker $faker) {
    return [
        'description' => $faker->sentence,
        'restaurant_id' => $faker->numberBetween(1, 10)
    ];
});
