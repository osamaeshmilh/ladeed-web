<?php

/** @var Factory $factory */

use App\Models\RestaurantReview;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(RestaurantReview::class, function (Faker $faker) {
    return [
        "review" => $faker->realText(),
        "rate" => $faker->randomFloat(2, 1, 5),
        "user_id" => $faker->numberBetween(1, 6),
        "restaurant_id" => $faker->numberBetween(1, 10),
    ];
});
