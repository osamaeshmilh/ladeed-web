<?php

/** @var Factory $factory */

use App\Models\Category;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement(['Vegetables', 'Sandwiches', 'Protein', 'Grains', 'Drinks']),
        'description' => $faker->sentences(5, true),
    ];
});
