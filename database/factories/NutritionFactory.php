<?php

/** @var Factory $factory */


use App\Models\Nutrition;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Nutrition::class, function (Faker $faker) {
    return [
        "name" => $faker->randomElement(["Sugar", "Proteins", "Lipid", "Calcium"]),
        "quantity" => $faker->randomFloat(2, 0.1, 200),
        "food_id" => $faker->numberBetween(1, 30),
    ];
});
