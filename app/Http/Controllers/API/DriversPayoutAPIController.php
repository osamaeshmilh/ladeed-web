<?php

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Models\DriversPayout;
use App\Repositories\DriversPayoutRepository;
use Flash;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class DriversPayoutController
 * @package App\Http\Controllers\API
 */
class DriversPayoutAPIController extends Controller
{
    /** @var  DriversPayoutRepository */
    private $driversPayoutRepository;

    public function __construct(DriversPayoutRepository $driversPayoutRepo)
    {
        $this->driversPayoutRepository = $driversPayoutRepo;
    }

    /**
     * Display a listing of the DriversPayout.
     * GET|HEAD /driversPayouts
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $this->driversPayoutRepository->pushCriteria(new RequestCriteria($request));
            $this->driversPayoutRepository->pushCriteria(new LimitOffsetCriteria($request));
        } catch (RepositoryException $e) {
            Flash::error($e->getMessage());
        }
        $driversPayouts = $this->driversPayoutRepository->all();

        return $this->sendResponse($driversPayouts->toArray(), 'Drivers Payouts retrieved successfully');
    }

    /**
     * Display the specified DriversPayout.
     * GET|HEAD /driversPayouts/{id}
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function show($id)
    {
        /** @var DriversPayout $driversPayout */
        if (!empty($this->driversPayoutRepository)) {
            $driversPayout = $this->driversPayoutRepository->findWithoutFail($id);
        }

        if (empty($driversPayout)) {
            return $this->sendError('Drivers Payout not found');
        }

        return $this->sendResponse($driversPayout->toArray(), 'Drivers Payout retrieved successfully');
    }
}
